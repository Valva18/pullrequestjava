package aytocalc;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Resta2Test {

	@Test
	void testRestaDosNumerosNegativos() {
		Resta2 resta = new Resta2();
		int rdo = resta.calcula(-4, -9);
		System.out.println("restaNumNega= " + rdo);
		assertEquals(5,0,rdo);
	}


}
